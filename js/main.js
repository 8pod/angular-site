/**
 * @author Denise Nordlöf
 */

/**
 * Main AngularJS Web Application
 */
var app = angular.module('tutorialWebApp', [
  'ngRoute'
]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "partials/home.html", controller: "PageCtrl"})
    // Pages
    .when("/all", {templateUrl: "partials/all.html", controller: "PageCtrl"})
    .when("/albums", {templateUrl: "partials/albums.html", controller: "PageCtrl"})
    .when("/about", {templateUrl: "partials/about.html", controller: "PageCtrl"})
    .when("/contact", {templateUrl: "partials/contact.html", controller: "PageCtrl"})
    .when("/manage", {templateUrl: "partials/manage.html", controller: "PageCtrl"})
    // else 404
    .otherwise({redirectTo : '/'});
}]);

/**
 * A template for controller to create dynamic data later on!
 */
app.controller('PageCtrl', function (/* $scope, $location, $http */) {
  console.log("PageCtrl loaded successfully");
});